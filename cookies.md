Ingrédients:
- 22g farine
- 1/2 sachet de levure chimique
- 100g sucre (blanc ou canne)
- 125g beurre salé, ou beurre puis rajouter sel
- 1cc miel
- 1 oeuf
- 60/70g pépittes chocolat

Préparation:
- Fondre beurre, mélanger avec miel et oeuf
- Dans autre récipient mélanger farine + sucre + lebure
- Tout mélanger, ajouter pépittes de chocolat
- 200°C 10min

Remarques:
- On peut rajouter du sucre vanillé ou de la vanille ou de la fleur d'oranger
- On peut rajouter une banane (pas forcément toute la banane) coupée finement
- On peut rajouter des noix, amandes ou noix du Brésil concassées / coupées
- On peut supprimer tous les ingrédients sucrées, et faire la recette avec:
    - compté + pignons de pin
    - parmesan + tomates séchées